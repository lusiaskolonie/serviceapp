<?php namespace ServiceCaller\ServiceCaller\Facades;

use Illuminate\Support\Facades\Facade;

class ServiceCaller extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'servicecurl'; }

}